<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Bus;

class RidesController extends Controller
{
  /**
  * List All Ride for a certain Bus.
  *
  * @return view
  */
  public function index(Bus $bus){

    return view('rides', compact('bus'));

  }
}
