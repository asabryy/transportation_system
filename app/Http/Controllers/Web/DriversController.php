<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DriversController extends Controller
{
  /**
  * List All Drivers.
  *
  * @return view
  */
  public function index(){

    $drivers = \App\Driver::all();

    return view('drivers', compact('drivers'));

  }
}
