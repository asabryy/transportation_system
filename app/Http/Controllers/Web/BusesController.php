<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BusesController extends Controller
{
  /**
  * List All Buses.
  *
  * @return view
  */
  public function index(){

    $buses = \App\Bus::all();

    return view('buses', compact('buses'));

  }



}
