<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChildrenController extends Controller
{
  /**
  * List All Children.
  *
  * @return view
  */
  public function index(){

    $children = \App\Child::all();

    return view('children', compact('children'));

  }
}
