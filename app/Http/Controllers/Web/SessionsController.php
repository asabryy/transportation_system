<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SessionsController extends Controller
{
  protected $loginPath = '/login';

  public function __construct(){

    $this->middleware('guest',['except' => 'destroy']);
  }

  public function create(){

    return view('authentication.login');
  }

  public function store(){

    if(!auth()->attempt(request(['email','password']))){

      return redirect('login')->withErrors([

        'message' => 'Please check your credentials and try again.'

      ]);
    }
     return redirect()->home();
  }

  public function destroy(){
    auth()->logout();
    return redirect()->route('login');
  }

}
