<?php

namespace App\Http\Controllers\Api;

use Request;
use \App\Child;
use \App\Bus;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class ChildrenController extends Controller
{
  /**
  * Register New Child.
  *
  * @return JSON RESPONSE (FAIL OR SUCCESS)
  */
  public function store(){
    //Validate Request Parameters
    $validation = Validator::make(Request::all(),[
      'name' => 'required|unique:children,name',
    ]);

    if($validation->fails()){
      $errors = $validation->errors();
      return $errors->toJson();
    }
    else{
      //Add new Bus
      $child = new Child;
      $child->name          = request('name');
      $child->photo_url     = request('photo_url');
      $child->date_of_birth = request('date_of_birth');
      $child->gender        = request('gender');
      $child->bus_id        = request('bus_id');

      if($child->save()){
        return response()->json([
          'message' => 'A NEW CHILD HAS BEEN ADDED SUCCESSFULLY.',
        ], 200); //HTTP STATUS CODE FOR SUCCESS
      }
      else{
        return response()->json([
          'message' => 'INTERNAL SERVER ERROR.',
        ], 500); //HTTP STATUS CODE FOR Server error
      }
    }

  }

  /**
  * Assign an Existing Child to an Existing Bus.
  *
  * @return JSON RESPONSE (FAIL OR SUCCESS)
  */
  public function assignToBus(Child $child, Bus $bus){

    //Validate the Bus is not full
    if($bus->number_of_seats > Child::where('bus_id', $bus->id)->count()){

      //Assign the required child
      $child->bus_id  = $bus->id;

      if($child->save()){
        return response()->json([
          'message' => 'CHILD '. $child->name . ' HAS BEEN ASSIGNED TO BUS '. $bus->plate_number . ' SUCCESSFULLY.',
        ], 200); //HTTP STATUS CODE FOR SUCCESS
      }
      else{
        return response()->json([
          'message' => 'INTERNAL SERVER ERROR.',
        ], 500); //HTTP STATUS CODE FOR Server error
      }
    }
    else{
      return response()->json([
        'message' => 'THIS BUS IS FULL.',
      ], 200); //HTTP STATUS CODE FOR Server error
    }
  }
}
