<?php

namespace App\Http\Controllers\Api;

use Request;
use \App\Bus;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class BusesController extends Controller
{
  /**
  * Register New Bus details.
  *
  * @return JSON RESPONSE (FAIL OR SUCCESS)
  */
  public function store(){
    //Validate Request Parameters
    $validation = Validator::make(Request::all(),[
      'plate_number'    => 'required|unique:buses,plate_number',
      'number_of_seats' => 'required|integer'
    ]);

    if($validation->fails()){
      $errors = $validation->errors();
      return $errors->toJson();
    }
    else{
      //Add new Bus
      $bus = new Bus;
      $bus->plate_number       = request('plate_number');
      $bus->number_of_seats    = request('number_of_seats');
      $bus->photo_url          = request('photo_url');
      $bus->make               = request('make');
      $bus->model              = request('model');
      $bus->year               = request('year');
      $bus->driver_id          = request('driver_id');
    
      if($bus->save()){
        return response()->json([
          'message' => 'A NEW BUS HAS BEEN ADDED SUCCESSFULLY.',
        ], 200); //HTTP STATUS CODE FOR SUCCESS
      }
      else{
        return response()->json([
          'message' => 'INTERNAL SERVER ERROR.',
        ], 500); //HTTP STATUS CODE FOR Server error
      }
    }

  }


}
