<?php

namespace App\Http\Controllers\Api;

use Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use \App\Ride;
use \App\Bus;
use \App\Place;

class RidesController extends Controller
{
  /**
  * Register New Ride details.
  *
  * @return JSON RESPONSE (FAIL OR SUCCESS)
  */
  public function store(){
    //Validate Request Parameters
    // dd(Request::all());
    $validation = Validator::make(Request::all(),[
      // 'origin_lat'       => 'required|numeric|between:0,99.99',
      // 'origin_lng'       => 'required|numeric|between:0,99.99',
      //
      // 'destination_lat'  => 'required|numeric|between:0,99.99',
      // 'destination_lng'  => 'required|numeric|between:0,99.99',
      "bus_id"    => "required|integer",
      "places"    => "required|array|min:2",
      // "places.*"  => "required|numeric|between:0,99.99",


    ]);

    if($validation->fails()){
      $errors = $validation->errors();
      return $errors->toJson();
    }
    else{
      //Add new RIDE
      $ride = new Ride;
      $ride->bus_id = request('bus_id');

      if($ride->save()){
        $places = request('places');

        foreach($places as $key=>$place){
          $new_place          = new Place;
          $new_place->lat     = $place['lat'];
          $new_place->lng     = $place['lng'];
          $new_place->order   = $key;
          $new_place->ride_id = $ride->id;
          $new_place->save();
        }
        return response()->json([
          'message' => 'A NEW RIDE HAS BEEN ADDED SUCCESSFULLY.',
        ], 200); //HTTP STATUS CODE FOR SUCCESS

      }
      else{
        return response()->json([
          'message' => 'INTERNAL SERVER ERROR.',
        ], 500); //HTTP STATUS CODE FOR Server error
      }
      //Add new places
      // $place1 = new Place;
      // $place1->lat = request('origin_lat');
      // $place1->lng = request('origin_lng');
      // // $place1->save();
      //
      // $place2 = new Place;
      // $place2->lat = request('destination_lat');
      // $place2->lng = request('destination_lng');
      // // $place2->save();
      //
      // if($place1->save() && $place2->save()){
      //   $ride = new Ride;
      //   $ride->bus_id         = request('bus_id');
      //   $ride->origin_id      = $place1->id;
      //   $ride->destination_id = $place2->id;
      //   if($ride->save() ){
      //     return response()->json([
      //       'message' => 'A NEW RIDE HAS BEEN ADDED SUCCESSFULLY.',
      //     ], 200); //HTTP STATUS CODE FOR SUCCESS
      //   }
      //   else{
      //     return response()->json([
      //       'message' => 'INTERNAL SERVER ERROR.',
      //     ], 500); //HTTP STATUS CODE FOR Server error
      //   }
    }
  }

  // }
}
