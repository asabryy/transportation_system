<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SessionsController extends Controller
{

  /**
  * Login Through API:  Store New Session for this User.
  *
  * @return JSON RESPONSE (FAIL OR SUCCESS with user data)
  */
  public function store(){

    //Check for Missing Parameters
    if(!request()->has('email') || !request()->has('password')){
      return response()->json([
        'error' => '400 Bad Request'
      ], 400); //HTTP STATUS CODE FOR MISSING PARAMETERS (BAD REQUEST)
    }

    //Validate parameters
    $this->validate(request(), [
      'email'     =>  'required|email',
      'password'  =>  'required|min:6'
    ]);

    if(auth()->attempt(request(['email','password']))){

      $user = auth()->user();

      $user->generateToken();

      return response()->json([
        'user'    => $user,
        'message' => 'success',
      ], 200); //HTTP STATUS CODE FOR SUCCESS
    }

    return response()->json([
      'message'   => 'Unauthorized'
    ], 401); //HTTP STATUS CODE FOR Unauthorized
  }

  /**
  * Logout: Destroy the User Session.
  *
  * @return JSON RESPONSE (FAIL OR SUCCESS)
  */
  public function destroy(){

    $user = \Auth::guard('api')->user();
    if ($user) {
        $user->api_token = null;
        $user->save();
    }
    return response()->json(['data' => 'User logged out.'], 200);

  }

}
