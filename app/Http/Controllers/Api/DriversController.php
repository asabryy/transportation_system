<?php

namespace App\Http\Controllers\Api;

use Request;
use \App\Driver;
use \App\User;
use \App\Bus;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class DriversController extends Controller
{
  /**
  * Register New Driver.
  *
  * @return JSON RESPONSE (FAIL OR SUCCESS)
  */
  public function store(){
    //Validate Request Parameters
    $validation = Validator::make(Request::all(),[
      'name' => 'required|unique:drivers,name',
      'ssn'  => 'required|integer',
      'driving_licence_number'  =>  'required',
      'email'     => 'required|email|max:255|unique:users',
      'password'  =>  'required|min:6'
    ]);

    if($validation->fails()){
      $errors = $validation->errors();
      return $errors->toJson();
    }
    else{
      //Add new user
      $user = new User;
      $user->name  = request('name');
      $user->email = request('email');
      $user->password = bcrypt(request('password'));
      $user->type_id  = 2; //driver

      if($user->save()){

        $driver = new Driver;
        $driver->name         = request('name');
        $driver->photo_url    = request('photo_url');
        $driver->ssn          = request('ssn');
        $driver->driving_licence_number  = request('driving_licence_number');
        $driver->user_id      = $user->id;
        $driver->save();
      }

      return response()->json([
        'message' => 'A NEW DRIVER HAS BEEN ADDED SUCCESSFULLY.',
      ], 200); //HTTP STATUS CODE FOR SUCCESS
    }

  }

  /**
  * Assign Driver to a bus.
  *
  * @return JSON RESPONSE (FAIL OR SUCCESS)
  */
  public function assignToBus(Driver $driver, Bus $bus){

    $bus->driver_id  = $driver->id;

    if($bus->save()){
      return response()->json([
        'message' => 'Driver '. $driver->name . ' HAS BEEN ASSIGNED TO BUS '. $bus->plate_number . ' SUCCESSFULLY.',
      ], 200); //HTTP STATUS CODE FOR SUCCESS
    }
    else{
      return response()->json([
        'message' => 'INTERNAL SERVER ERROR.',
      ], 500); //HTTP STATUS CODE FOR Server error
    }
  }
}
