<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ride extends Model
{
    public function bus(){
      return $this->belongsTo(Bus::class);
    }

    public function places(){
      return $this->hasMany(Place::class);
    }
}
