<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bus extends Model
{
    public function driver(){
      return $this->belongsTo(Driver::class);
    }

    public function children()
    {
      return $this->hasMany(Child::class);
    }

    public function rides(){
      return $this->hasMany(Ride::class);
    }

    public function ridesWithPlaces(){
      return Ride::with('places')->where('bus_id',$this->id)->get();
    }
}
