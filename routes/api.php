<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::group([
  'namespace'  => 'Api'
], function () {

  Route::POST('/login','SessionsController@store');

});

Route::group([
  // 'middleware' => ['auth:api'],
  'namespace'  => 'Api'
], function () {

  Route::post('buses', 'BusesController@store');

  Route::post('rides', 'RidesController@store');

  Route::post('children', 'ChildrenController@store');
  Route::get('children/{child}/{bus}', 'ChildrenController@assignToBus');

  Route::post('drivers', 'DriversController@store');
  Route::get('drivers/{driver}/{bus}', 'DriversController@assignToBus');

  Route::GET('logout','SessionsController@destroy');

});
