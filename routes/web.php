<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//Login
Route::get('/login', 'Web\SessionsController@create')->name('login');
Route::post('/login', 'Web\SessionsController@store');
Route::get('/logout', 'Web\SessionsController@destroy');
// Route::get('/', function () {
//   return view('map');
// })->name('map');

Route::group([
  // 'middleware' => ['auth:web'],
  'namespace'  => 'Web'
], function () {

  Route::get('/', function () {
    return view('dashboard');
  })->name('home');

  Route::get('drivers', 'DriversController@index');
  Route::get('buses', 'BusesController@index');
  Route::get('buses/{bus}', 'RidesController@index');
  Route::get('children', 'ChildrenController@index');

});
