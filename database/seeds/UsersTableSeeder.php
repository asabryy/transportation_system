<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    //The Admin User Login
    DB::table('users')->insert([
      'name' => 'Admin',
      'email' => 'admin@transportation-system.com',
      'password' => bcrypt('123456'),
    ]);
  }
}
