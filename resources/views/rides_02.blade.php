@extends('app')


@section('content')

  <h2>Rides for Bus #{{ $bus->plate_number }}</h2>
  <div class="table-responsive">
    <table class="table table-striped table-sm">
      <thead>
        <tr>
          <th>#</th>
          <th>Origin</th>
          <th>Destination</th>
          <th>Created At</th>
        </tr>
      </thead>
      <tbody>
        @foreach($bus->rides as $ride)
        <tr>
          <td>{{ $ride->id }}</td>
          <td>{{ $ride->origin }}</td>
          <td>{{ $ride->destination }}</td>
          <td>{{ $ride->created_at->toFormattedDateString() }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
@endsection
