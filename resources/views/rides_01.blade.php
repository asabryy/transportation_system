@extends('app')


@section('content')
<style media="screen">
#map {
  /* height: 100%; */
  width: 100px;
}
</style>
<h2>Rides for Bus #{{ $bus->plate_number }}</h2>
@foreach($bus->rides as $ride)
<div id="map"></div>

@endforeach
<div class="table-responsive">
  <table class="table table-striped table-sm">
    <thead>
      <tr>
        <th>#</th>
        <th>Origin</th>
        <th>Destination</th>
        <th>Created At</th>
      </tr>
    </thead>
    <tbody>
      @foreach($bus->rides as $ride)
      <tr>
        <td>{{ $ride->id }}</td>
        <td>{{ $ride->origin }}</td>
        <td>{{ $ride->destination }}</td>
        <td>{{ $ride->created_at->toFormattedDateString() }}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>

<script>

// This example creates a 2-pixel-wide red polyline showing the path of
// the first trans-Pacific flight between Oakland, CA, and Brisbane,
// Australia which was made by Charles Kingsford Smith.

function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 3,
    center: {lat: 0, lng: -180},
    mapTypeId: 'terrain'
  });

  var flightPlanCoordinates = [
    {lat: 37.772, lng: -122.214},
    {lat: 21.291, lng: -157.821},
    {lat: -18.142, lng: 178.431},
    {lat: -27.467, lng: 153.027}
  ];
  var flightPath = new google.maps.Polyline({
    path: flightPlanCoordinates,
    geodesic: true,
    strokeColor: '#FF0000',
    strokeOpacity: 1.0,
    strokeWeight: 2
  });

  flightPath.setMap(map);
}
</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCG2iPXRIFJ_ah2S45WyfMKT41fldwpJhY&callback=initMap">
</script>
@endsection
