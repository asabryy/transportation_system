@extends('app')


@section('content')

  <h2>Drivers</h2>
  <div class="table-responsive">
    <table class="table table-striped table-sm">
      <thead>
        <tr>
          <th>#</th>
          <th>Name</th>
          <th>SSN</th>
          <th>Driving Licence Number</th>
        </tr>
      </thead>
      <tbody>
        @foreach($drivers as $driver)
        <tr>
          <td>{{ $driver->id }}</td>
          <td>{{ $driver->name }}</td>
          <td>{{ $driver->ssn }}</td>
          <td>{{ $driver->driving_licence_number }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
@endsection
