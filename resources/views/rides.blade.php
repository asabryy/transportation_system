@extends('app')


@section('content')
<style media="screen">
.map {
  height: 300px;
  width: 300px;
  border: 5px solid white;

}

</style>
<h2>Rides for Bus #{{ $bus->plate_number }}</h2>

@foreach($bus->rides as $key=>$ride)
  <h3> Ride #{{$key}}</h3>
  <div class="map" data-id="{{$key}}"></div>
@endforeach


<script>
<?php
$json=json_encode( $bus->ridesWithPlaces() );
/* create javascript variable with data */
echo "var json={$json};";
?>
// This example creates a 2-pixel-wide red polyline showing the path of
// the first trans-Pacific flight between Oakland, CA, and Brisbane,
// Australia which was made by Charles Kingsford Smith.

function initMap() {
  for( var n in json ){
    obj=json[n];
    var map = new google.maps.Map(document.querySelector('div[ data-id="'+n+'" ]'), {
      zoom: 3,
      center: {lat: 0, lng: -180},
      mapTypeId: 'terrain'
    });

    var flightPlanCoordinates = [];
     // here's your object

    obj.places.forEach(function(item,index) {
      mylocation = {};
      mylocation ["lat"] = parseFloat(item.lat);
      mylocation ["lng"] = parseFloat(item.lng); // you modify it's properties
      flightPlanCoordinates.push(mylocation); // you push it to the array
    });
    // console.log(flightPlanCoordinates);

    var flightPath = new google.maps.Polyline({
      path: flightPlanCoordinates,
      geodesic: true,
      strokeColor: '#FF0000',
      strokeOpacity: 1.0,
      strokeWeight: 2
    });

    flightPath.setMap(map);
  }
}
</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCG2iPXRIFJ_ah2S45WyfMKT41fldwpJhY&callback=initMap">
</script>
@endsection
