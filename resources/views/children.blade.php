@extends('app')


@section('content')

  <h2>Children</h2>
  <div class="table-responsive">
    <table class="table table-striped table-sm">
      <thead>
        <tr>
          <th>#</th>
          <th>Name</th>
          <th>Date Of Birth</th>
          <th>Gender</th>
          <th>Bus</th>
        </tr>
      </thead>
      <tbody>
        @foreach($children as $child)
        <tr>
          <td>{{ $child->id }}</td>
          <td>{{ $child->name }}</td>
          <td>{{ $child->date_of_birth }}</td>
          <td>{{ $child->gender }}</td>
          <td>{{ $child->bus ?  $child->bus->plate_number : 'unassigned' }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
@endsection
