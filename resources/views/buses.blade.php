@extends('app')


@section('content')

<h2>Buses</h2>
<div class="table-responsive">
  <table class="table table-striped table-sm">
    <thead>
      <tr>
        <th>#</th>
        <th>Plate Number</th>
        <th>Driver Name</th>
        <th>Number Of Seats</th>
      </tr>
    </thead>
    <tbody>
      @foreach($buses as $bus)
      <tr>
        <td>{{ $bus->id }}</td>
        <td>
          <a href="{{ url('buses/' . $bus->id) }}">
            {{ $bus->plate_number }}
          </a>
        </td>
        <td>{{ $bus->driver ? $bus->driver->name : '' }}</td>
        <td>{{ $bus->number_of_seats }}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
@endsection
